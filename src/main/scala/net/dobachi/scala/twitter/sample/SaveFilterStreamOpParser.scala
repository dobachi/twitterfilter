package net.dobachi.scala.twitter.sample

/**
 * Definition of command line arguments and it's default values.
 */
case class SaveFilterStreamOpParserConfig(
                                     keyWord: String = "カレー",
                                     time: Int = 10000,
                                     outputDir: String = "/tmp/tweets")
/**
 * Detail of command line arguments.
 */
@SerialVersionUID(1L)
class SaveFilterStreamOpParser() extends Serializable {
  val parser = new scopt.OptionParser[SaveFilterStreamOpParserConfig]("SaveFilterStream") {
    arg[String]("keyWord") required() action {
      (x, c) => c.copy(keyWord = x)
    } text("The keyword delimited by comma")

    arg[Int]("time") required() action {
      (x, c) => c.copy(time = x)
    } text("The time span to obtain stream")

    opt[String]('o', "outputDir") valueName("PATH") action {
      (x, c) => c.copy(outputDir = x)
    } text("The path of output dir")

  }

  def getParser() = {
    parser
  }
}

object SaveFilterStreamOpParser {
  val SaveFilterStreamOpParser = new SaveFilterStreamOpParser()

  def apply() = {
    SaveFilterStreamOpParser
  }
}