package net.dobachi.scala.twitter.sample

import java.io._
import twitter4j._

@SerialVersionUID(1L)
class Util(baseDir: String) extends Serializable {
}

/**
 * Created by dobachi on 14/09/29.
 */
object Util {


  val config = new twitter4j.conf.ConfigurationBuilder()
    .setOAuthConsumerKey("<Your ConsumerKey>")
    .setOAuthConsumerSecret("<Your ConsumerSeretKey>")
    .setOAuthAccessToken("<Your AccessToken>")
    .setOAuthAccessTokenSecret("<Your AccessTokenSecret>")
    .build

  def simpleStatusListener = new StatusListener() {
    def onStatus(status: Status) { println(status.getText) }
    def onDeletionNotice(statusDeletionNotice: StatusDeletionNotice) {}
    def onTrackLimitationNotice(numberOfLimitedStatuses: Int) {}
    def onException(ex: Exception) { ex.printStackTrace }
    def onScrubGeo(arg0: Long, arg1: Long) {}
    def onStallWarning(warning: StallWarning) {}
  }

  def saveStreamStatusListener(baseDir: String) =
    new StatusListener() {
      def onStatus(status: Status) = saveStream(status, baseDir)

      def onDeletionNotice(statusDeletionNotice: StatusDeletionNotice) {}

      def onTrackLimitationNotice(numberOfLimitedStatuses: Int) {}

      def onException(ex: Exception) {
        ex.printStackTrace
      }

      def onScrubGeo(arg0: Long, arg1: Long) {}

      def onStallWarning(warning: StallWarning) {}
    }

  def saveStream(status: Status, baseDir: String) = {
    val rawJSON = TwitterObjectFactory.getRawJSON(status)
    val fileName = getFileName(status, baseDir)

    println("save as : " + fileName)
    storeJSON(rawJSON, fileName)
  }

  def getFileName(status: Status, baseDir: String) = new File(baseDir, status.getId() + ".json").getPath

  def storeJSON(rawJSON: String, fileName: String) = {
    using(new FileOutputStream(fileName)) { fos =>
      using(new OutputStreamWriter(fos, "UTF-8")) { osw =>
        using(new BufferedWriter(osw)) { bw =>
          bw.write(rawJSON)
          bw.flush()
        }
      }
    }
  }

  def using[A <% { def close():Unit }](s: A)(f: A=>Any) {
    try {
      f(s)
    } finally {
      try {
        s.close()
      } catch {
        case ignore: IOException => {}
      }
    }
  }
}


