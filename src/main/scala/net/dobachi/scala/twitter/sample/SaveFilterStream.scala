package net.dobachi.scala.twitter.sample

import java.io.File

import twitter4j._

object SaveFilterStream {
  def main(args: Array[String]): Unit = {

    val saveFilterStreamOpParser = SaveFilterStreamOpParser()

    saveFilterStreamOpParser.getParser().parse(args, SaveFilterStreamOpParserConfig()) exists { config =>

      val twitterStream = new TwitterStreamFactory(Util.config).getInstance
      println("Saving twitter stream")

      new File(config.outputDir).mkdir
      twitterStream.addListener(Util.saveStreamStatusListener(config.outputDir))

      val keyWords = config.keyWord.split(",").map(_.trim)
      val filterQuery = new FilterQuery()
      println("keyWords: " + keyWords.mkString(","))
      filterQuery.track(keyWords)
      twitterStream.filter(filterQuery)
      Thread.sleep(config.time)
      twitterStream.cleanUp
      twitterStream.shutdown

      true
    }
  }
}
